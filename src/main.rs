use std::fs;
use regex::Regex;

mod operation_parser {
    fn operand(operand: &str, old: i64) -> i64 {
        match operand {
            "old" => old,
            _ => String::from(operand).parse::<i64>().unwrap(),
        }
    }

    fn operation(operator: &str, operand1: i64, operand2: i64) -> i64 {
        match operator {
            "+" => operand1 + operand2,
            "-" => operand1 - operand2,
            "*" => operand1 * operand2,
            "/" => operand1 / operand2,
            _ => 0
        }
    }

    pub fn calculate_worry_level(expression: &String, item: i64) -> i64 {
        let arr = expression.split(' ').collect::<Vec<&str>>();
        let mut res = operand(arr[0], item);
        for i in (1..arr.len()).step_by(2) {
            res = operation(arr[i], res, operand(arr[i + 1], item));
        }
        res
    }
}

#[derive(Debug)]
#[derive(Default)]
struct Monkey {
    items: Vec<i64>,
    operation: String,
    divisor: i64,
    monkey1: usize,
    monkey2: usize,
    counter: i64,
}

impl Monkey {
    fn push_item(&mut self, item: i64) {
        self.items.push(item);
    }

    fn pop_item(&mut self) -> i64 {
        self.counter += 1;
        self.items.pop().unwrap()
    }
}

fn load_data() -> Vec<Monkey> {
    let file_path = "11real.txt";
    let contents = fs::read_to_string(file_path)
        .expect("File not found");

    let re = Regex::new(r"Monkey +\d+:\s*
\s+Starting items: (\d+(?:, \d+)*)\s*
\s+Operation: new = ([^\r\n]*)\s*
\s+Test: divisible by (\d+)\s*
\s+If true: throw to monkey (\d+)\s*
\s+If false: throw to monkey (\d+)(\s*\n+)*")
        .unwrap();

    let mut monkeys: Vec<Monkey> = Vec::new();

    for cap in re.captures_iter(&contents) {
        //println!("{}\n{}\n{}\n{}\n{}\n", &cap[1], &cap[2], &cap[3], &cap[4], &cap[5]);
        let items = &mut cap[1]
            .replace(" ", "")
            .split(",")
            .map(|x| x.parse::<i64>().unwrap())
            .collect::<Vec<i64>>();
        items.reverse();
        let operation = &cap[2];
        let divisor = &cap[3].parse::<i64>().unwrap();
        let div_true = &cap[4].parse::<usize>().unwrap();
        let div_false = &cap[5].parse::<usize>().unwrap();

        let monkey: Monkey = Monkey {
            items: items.clone(),
            operation: String::from(operation),
            divisor: *divisor,
            monkey1: *div_true,
            monkey2: *div_false,
            ..Default::default()
        };
        monkeys.push(monkey);
    }
    monkeys
}

fn round(monkeys: &mut Vec<Monkey>) {
    for i in 0..monkeys.len() {
        for _ in 0..monkeys[i].items.len() {
            let item = monkeys[i].pop_item();
            let worry_level = operation_parser::calculate_worry_level(&monkeys[i].operation, item) / 3;
            if worry_level % monkeys[i].divisor == 0 {
                let monkey1 = monkeys[i].monkey1;
                monkeys[monkey1].push_item(worry_level);
            } else {
                let monkey2 = monkeys[i].monkey2;
                monkeys[monkey2].push_item(worry_level);
            }
        }
    }
}

fn print_statistics(monkeys: &Vec<Monkey>) {
    let mut most1 = 0;
    let mut most2 = 0;
    for i in 0..monkeys.len() {
        let m = &monkeys[i];
        println!("Monkey {} inspected items {} times.", i, m.counter);
        if m.counter > most1 {
            most2 = most1;
            most1 = m.counter;
        } else if m.counter > most2 {
            most2 = m.counter;
        }
    }
    println!("The level of monkey business is {}", most1 * most2);
}

fn main() {
    let mut monkeys = load_data();
    for _ in 0..20 {
        round(&mut monkeys);
    }
    //println!("{:?}", monkeys);
    print_statistics(&monkeys);

}
